package com.joshafeinberg.workouttracker.database;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import java.lang.String;

public class IndividualWorkoutTable {
    public static final String COLUMN_EXERCISE_ID = "ExerciseID";
    public static final String COLUMN_INDV_WORKOUT_ID = "IndvWorkoutID";
    public static final String COLUMN_WORKOUT_ID = "WorkoutID";
    public static final String COLUMN_WORKOUT_REPS = "WorkoutReps";
    public static final String COLUMN_WORKOUT_TIME = "WorkoutTime";
    public static final String COLUMN_WORKOUT_WEIGHT = "WorkoutWeight";
    public static final String TABLE_NAME = "IndividualWorkouts";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL("CREATE TABLE " + TABLE_NAME + "(" +
                COLUMN_INDV_WORKOUT_ID + " integer primary key autoincrement, " +
                COLUMN_WORKOUT_ID + " integer, " +
                COLUMN_EXERCISE_ID + " integer, " +
                COLUMN_WORKOUT_REPS + " integer, " +
                COLUMN_WORKOUT_WEIGHT + " integer, " +
                COLUMN_WORKOUT_TIME + " integer, " +
                "FOREIGN KEY(" + COLUMN_WORKOUT_ID + ") REFERENCES " + WorkoutTable.TABLE_NAME + "(" + WorkoutTable.COLUMN_ID + ") ON DELETE CASCADE, " +
                "FOREIGN KEY(" + COLUMN_EXERCISE_ID + ") REFERENCES " + ExerciseTable.TABLE_NAME + "(" + ExerciseTable.COLUMN_ID + "));");
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        Log.w(WorkoutExercisesTable.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS IndividualWorkouts");
        IndividualWorkoutTable.onCreate(database);
    }
}

