package com.joshafeinberg.workouttracker.libraries;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.joshafeinberg.workouttracker.R;
import com.joshafeinberg.workouttracker.models.Exercise;

import java.util.List;

public class ExerciseListAdapter extends ArrayAdapter<Exercise.ExerciseItem> {
    OnItemRemove callback;
    private int resource;

    public ExerciseListAdapter(Context context, int resource, List<Exercise.ExerciseItem> items) {
        super(context, resource, items);
        this.resource = resource;
    }


    public View getView(int row, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final Exercise.ExerciseItem exerciseItem = getItem(row);
        View rowView = view;
        if (exerciseItem.exerciseID.equals("ADD")) {
            View addExerciseRow = inflater.inflate(R.layout.new_workout_exercise_list_add_item, null);
            if (addExerciseRow != null) {
                addExerciseRow.findViewById(R.id.newWorkoutAddExercise).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ExerciseListAdapter.this.callback.addExercise();
                    }
                });
            }
            return addExerciseRow;
        }

        if (rowView == null || rowView.findViewById(R.id.exerciseListItemName) == null) {
            rowView = inflater.inflate(this.resource, null);
        }

        if (rowView != null) {
            TextView exerciseName = (TextView) rowView.findViewById(R.id.exerciseListItemName);
            if (exerciseName != null) {
                exerciseName.setText(exerciseItem.exerciseName);
            }

            ImageButton exerciseRemove = (ImageButton) rowView.findViewById(R.id.exerciseListItemRemove);
            if (exerciseRemove != null) {
                exerciseRemove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callback.removeExercise(exerciseItem);
                    }
                });
            }
        }

        return rowView;
    }

    public void setCallback(OnItemRemove onItemRemove) {
        this.callback = onItemRemove;
    }

    public interface OnItemRemove {
        public void addExercise();

        public void removeExercise(Exercise.ExerciseItem exerciseItem);
    }

}

