package com.joshafeinberg.workouttracker.database;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import java.lang.String;

public class ExerciseTable {
    public static final String COLUMN_ID = "ExerciseID";
    public static final String COLUMN_NAME = "ExerciseName";
    public static final String TABLE_NAME = "Exercises";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL("CREATE TABLE " + TABLE_NAME + "(" +
                COLUMN_ID + " integer primary key autoincrement, " +
                COLUMN_NAME + " text);");
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        Log.w(WorkoutExercisesTable.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS Exercises");
        ExerciseTable.onCreate(database);
    }
}

