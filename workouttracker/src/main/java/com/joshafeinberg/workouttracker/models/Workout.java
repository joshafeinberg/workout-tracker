package com.joshafeinberg.workouttracker.models;

import android.content.Context;
import com.joshafeinberg.workouttracker.database.DatabaseHelper;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Workout {
    public Map<String, WorkoutItem> workoutMap;
    public List<WorkoutItem> workouts;
    public static enum workoutTypes {MAX, REPS}

    public Workout(Context context) {
        this.workouts = new DatabaseHelper(context).getAllWorkouts();
        this.workoutMap = new HashMap<String, WorkoutItem>();
        for (WorkoutItem workoutItem : this.workouts) {
            this.workoutMap.put(workoutItem.workoutID, workoutItem);
        }
    }

    public static class WorkoutItem {
        public String workoutID;
        public String workoutName;
        public workoutTypes workoutType;
        public long lastWorkout;


        public WorkoutItem(int workoutID, String workoutName, int workoutType, long lastWorkout) {
            this.workoutID = Integer.toString(workoutID);
            this.workoutName = workoutName;
            this.workoutType = workoutTypes.values()[workoutType];
            this.lastWorkout = lastWorkout;
        }

        public WorkoutItem(int workoutID, String workoutName, workoutTypes workoutType, long lastWorkout) {
            this.workoutID = Integer.toString(workoutID);
            this.workoutName = workoutName;
            this.workoutType = workoutType;
            this.lastWorkout = lastWorkout;
        }

        public WorkoutItem(String workoutName, workoutTypes workoutType, long lastWorkout) {
            this.workoutName = workoutName;
            this.workoutType = workoutType;
            this.lastWorkout = lastWorkout;
        }

        public String toString() {
            return this.workoutName;
        }
    }


}

