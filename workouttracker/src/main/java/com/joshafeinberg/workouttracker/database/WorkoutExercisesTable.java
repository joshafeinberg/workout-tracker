package com.joshafeinberg.workouttracker.database;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import java.lang.String;

public class WorkoutExercisesTable {
    public static final String COLUMN_EXERCISE_ID = "ExerciseID";
    public static final String COLUMN_WORKOUT_ID = "WorkoutID";
    public static final String TABLE_NAME = "WorkoutExercises";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL("CREATE TABLE " + TABLE_NAME + "(" +
                COLUMN_WORKOUT_ID + " integer, " +
                COLUMN_EXERCISE_ID + " integer, " +
                "FOREIGN KEY(" + COLUMN_WORKOUT_ID + ") REFERENCES " + WorkoutTable.TABLE_NAME + "(" + WorkoutTable.COLUMN_ID + ") ON DELETE CASCADE, " +
                "FOREIGN KEY(" + COLUMN_EXERCISE_ID + ") REFERENCES " + ExerciseTable.TABLE_NAME + "(" + ExerciseTable.COLUMN_ID + "))");

    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        Log.w(WorkoutExercisesTable.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        WorkoutExercisesTable.onCreate(database);
    }
}

