
package com.joshafeinberg.workouttracker;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.joshafeinberg.workouttracker.database.DatabaseHelper;
import com.joshafeinberg.workouttracker.libraries.ExerciseListAdapter;
import com.joshafeinberg.workouttracker.models.Exercise;
import com.joshafeinberg.workouttracker.models.Workout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddWorkoutFragment extends Fragment implements ExerciseListAdapter.OnItemRemove {
    private static Callbacks itemAddedCallback = new Callbacks(){

        @Override
        public void onItemSelected(String string) {
        }

        @Override
        public void updateAdapter() {
        }
    };
    private ExerciseListAdapter adapter;
    private List<Exercise.ExerciseItem> exerciseItems;
    private ListView exercises;
    private Callbacks mCallbacks = AddWorkoutFragment.itemAddedCallback;

    public static String WORKOUT_ITEM_ID = "workout_id";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(WORKOUT_ITEM_ID)) {
            Workout.WorkoutItem mItem = new DatabaseHelper(getActivity()).getWorkout(getArguments().getString(WORKOUT_ITEM_ID));
            if (getActivity().getActionBar() != null) {
                getActivity().getActionBar().setTitle(mItem.workoutName);
            }
        }

        this.setRetainInstance(true);
        this.setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View view = layoutInflater.inflate(R.layout.add_workout, viewGroup, false);

        if (view != null) {
            this.exercises = (ListView) view.findViewById(R.id.newWorkoutExercises);
            if (this.exerciseItems == null) {
                this.exerciseItems = new ArrayList<Exercise.ExerciseItem>();
                this.exerciseItems.add(new Exercise.ExerciseItem("ADD", "", 0, 0));
            }
            this.adapter = new ExerciseListAdapter(getActivity(), R.layout.new_workout_exercise_list_item, this.exerciseItems);
            this.adapter.setCallback(this);
            this.exercises.setAdapter(this.adapter);
            setListViewHeightBasedOnChildren(this.exercises);
            view.findViewById(R.id.newWorkoutAddButton).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addWorkout();
                }
            });
        }
        return view;
    }


    @Override
    public void onDetach() {
        super.onDetach();
        this.mCallbacks = null;
    }


    public void addExercise() {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View view = inflater.inflate(R.layout.add_exercise, null);
        if (view == null) {
            return;
        }

        ArrayList<String> usedExercises = new ArrayList<String>();
        for (Exercise.ExerciseItem exerciseItem : this.exerciseItems) {
            usedExercises.add(exerciseItem.exerciseID);
        }

        final DatabaseHelper databaseHelper = new DatabaseHelper(getActivity());
        final Spinner newWorkoutSpinner = (Spinner) view.findViewById(R.id.newWorkoutAddExerciseSpinner);
        final List<Exercise.ExerciseItem> allExercises = databaseHelper.getAllExercises();
        ArrayList<Exercise.ExerciseItem> unusedExerciseItems = new ArrayList<Exercise.ExerciseItem>(allExercises);
        List<Map<String, String>> spinnerItems = new ArrayList<Map<String, String>>();

        HashMap<String, String> newItem = new HashMap<String, String>();
        newItem.put("exerciseName", getString(R.string.newworkoutaddnew));
        spinnerItems.add(newItem);
        for (Exercise.ExerciseItem exerciseItem : allExercises) {
            if (usedExercises.indexOf(exerciseItem.exerciseID) == -1) {
                HashMap<String, String> spinnerItem = new HashMap<String, String>();
                spinnerItem.put("exerciseName", exerciseItem.exerciseName);
                spinnerItems.add(spinnerItem);
            } else {
                unusedExerciseItems.remove(exerciseItem);
            }

        }

        String[] from = new String[]{"exerciseName"};
        int[] to = new int[]{android.R.id.text1};
        newWorkoutSpinner.setAdapter(new SimpleAdapter(getActivity(), spinnerItems, android.R.layout.simple_spinner_item, from, to));

        final EditText newWorkoutExerciseName = (EditText) view.findViewById(R.id.newWorkoutAddExerciseText);
        newWorkoutSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    newWorkoutExerciseName.setVisibility(View.GONE);
                } else {
                    newWorkoutExerciseName.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.newworkoutaddexercise);
        builder.setView(view);
        builder.setPositiveButton(R.string.newworkoutaddexercise, null);
        builder.setNegativeButton(android.R.string.cancel, null);
        final AlertDialog dialog = builder.create();
        dialog.show();
        final ArrayList<Exercise.ExerciseItem> fUnusedExerciseItems = unusedExerciseItems;
        Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        if (positiveButton != null) {
            positiveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int selected = newWorkoutSpinner.getSelectedItemPosition();
                    if (newWorkoutExerciseName.getText() != null) {
                        if (newWorkoutExerciseName.getText().toString().equals("") && selected == 0) {
                            Toast.makeText(getActivity(), R.string.exerciseerror, Toast.LENGTH_LONG).show();
                            return;
                        }
                    }
                    Exercise.ExerciseItem exerciseItem;
                    if (selected == 0) {
                        String newExerciseName = newWorkoutExerciseName.getText().toString();
                        long newExercise = databaseHelper.addExercise(newExerciseName, "");
                        exerciseItem = new Exercise.ExerciseItem(Long.toString(newExercise), newExerciseName, 0, 0);
                    } else {
                        exerciseItem = fUnusedExerciseItems.get(selected - 1);
                    }

                    Exercise.ExerciseItem addButton = exerciseItems.remove(exerciseItems.size() - 1);
                    exerciseItems.add(exerciseItem);
                    exerciseItems.add(addButton);
                    adapter.notifyDataSetChanged();
                    setListViewHeightBasedOnChildren(exercises);
                    dialog.dismiss();
                }
            });

        }

    }

    public void addWorkout() {
        View view = getView();

        EditText newWorkoutName = (EditText) view.findViewById(R.id.newWorkoutName);

        String newWorkoutNameString = "";
        if (newWorkoutName.getText() != null) {
            newWorkoutNameString = newWorkoutName.getText().toString();
        }
        if (newWorkoutNameString.equals("")) {
            Toast.makeText(getActivity(), R.string.workoutnameerror, Toast.LENGTH_LONG).show();
            return;
        }

        RadioGroup workoutType = (RadioGroup) view.findViewById(R.id.newWorkoutType);
        Workout.workoutTypes newWorkoutType = null;
        switch (workoutType.getCheckedRadioButtonId()) {
            case R.id.newWorkoutTypeReps:
                newWorkoutType = Workout.workoutTypes.REPS;
                break;
            case R.id.newWorkoutTypeWeight:
                newWorkoutType = Workout.workoutTypes.MAX;
                break;
        }
        if (newWorkoutType == null) {
            Toast.makeText(getActivity(), R.string.workouttypeerror, Toast.LENGTH_LONG).show();
            return;
        }

        if (exerciseItems.size() <= 1) {
            Toast.makeText(getActivity(), R.string.workoutexerciseerror, Toast.LENGTH_LONG).show();
            return;
        }

        DatabaseHelper databaseHelper = new DatabaseHelper(getActivity());
        long workoutID = databaseHelper.addWorkout(newWorkoutName.getText().toString(), newWorkoutType);
        for (Exercise.ExerciseItem exerciseItem : this.exerciseItems) {
            if (!exerciseItem.exerciseID.equals("ADD")) {
                databaseHelper.addWorkoutExercise(Long.toString(workoutID), exerciseItem.exerciseID);
            }
        }

        this.mCallbacks.onItemSelected(Long.toString(workoutID));
        this.mCallbacks.updateAdapter();
    }



    @Override
    public void removeExercise(Exercise.ExerciseItem exerciseItem) {
        this.exerciseItems.remove(exerciseItem);
        this.adapter.notifyDataSetChanged();
        AddWorkoutFragment.setListViewHeightBasedOnChildren(this.exercises);
    }

    public void setCallback(Callbacks callbacks) {
        this.mCallbacks = callbacks;
    }

    public interface Callbacks {
        public void onItemSelected(String var1);

        public void updateAdapter();
    }



    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }

        int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            if (listItem instanceof ViewGroup) {
                listItem.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            }
            if (listItem != null) {
                listItem.measure(0, 0);
                totalHeight += listItem.getMeasuredHeight();
            }
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        if (params != null) {
            params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        }
        listView.setLayoutParams(params);
    }
}

