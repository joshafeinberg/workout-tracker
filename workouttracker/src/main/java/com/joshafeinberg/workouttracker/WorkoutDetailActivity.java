package com.joshafeinberg.workouttracker;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;

import com.joshafeinberg.workouttracker.database.DatabaseHelper;
import com.joshafeinberg.workouttracker.models.Workout;

public class WorkoutDetailActivity extends FragmentActivity {
    private WorkoutDetailFragment workoutDetailFragment;
    private String workoutID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_workout_detail);
        this.workoutID = this.getIntent().getStringExtra("workout_id");
        Workout.WorkoutItem workoutItem = new DatabaseHelper(getApplicationContext()).getWorkout(this.workoutID);

        if (getActionBar() != null) {
            getActionBar().setDisplayHomeAsUpEnabled(true);
            getActionBar().setTitle(workoutItem.workoutName);
        }

        if (savedInstanceState == null) {
            Bundle myBundle = new Bundle();
            myBundle.putString("workout_id", this.getIntent().getStringExtra("workout_id"));
            workoutDetailFragment = new WorkoutDetailFragment();
            workoutDetailFragment.setArguments(myBundle);
        } else {
            workoutDetailFragment = (WorkoutDetailFragment)(getSupportFragmentManager().findFragmentByTag("details"));
        }

        getSupportFragmentManager().beginTransaction().add(R.id.workout_detail_container, workoutDetailFragment, "details").commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.getMenuInflater().inflate(R.menu.workout_list_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpTo(this, new Intent(this, WorkoutListActivity.class));
                break;
            case R.id.action_editWorkout:
                this.editWorkout();
                break;
            case R.id.action_removeWorkout:
                WorkoutListActivity.removeWorkout(this, workoutID);
                NavUtils.navigateUpTo(this, new Intent(this, WorkoutListActivity.class));
                break;
            default:
                return super.onOptionsItemSelected(menuItem);
        }

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem addWorkout = menu.findItem(R.id.action_addWorkout);
        if (addWorkout != null) {
            addWorkout.setVisible(false);
        }
        MenuItem manageExercises = menu.findItem(R.id.action_manageExercises);
        if (manageExercises != null) {
            manageExercises.setVisible(false);
        }
        MenuItem preferences = menu.findItem(R.id.action_preferences);
        if (preferences != null) {
            preferences.setVisible(false);
        }
        return true;
    }

    public void editWorkout() {
        if (workoutDetailFragment != null) {
            workoutDetailFragment.editWorkout();
        }
    }
}

