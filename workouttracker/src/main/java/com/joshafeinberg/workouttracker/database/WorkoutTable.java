
package com.joshafeinberg.workouttracker.database;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import java.lang.String;

public class WorkoutTable {
    public static final String COLUMN_ID = "WorkoutID";
    public static final String COLUMN_NAME = "WorkoutName";
    public static final String COLUMN_TYPE = "WorkoutType";
    public static final String TABLE_NAME = "Workouts";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL("CREATE TABLE " + TABLE_NAME + "(" +
                COLUMN_ID + " integer primary key autoincrement, " +
                COLUMN_NAME + " text, " +
                COLUMN_TYPE + "  integer);");
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        Log.w(WorkoutExercisesTable.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS" + TABLE_NAME);
        WorkoutTable.onCreate(database);
    }
}

