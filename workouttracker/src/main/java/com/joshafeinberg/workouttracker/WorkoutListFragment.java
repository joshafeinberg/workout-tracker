package com.joshafeinberg.workouttracker;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.joshafeinberg.workouttracker.models.Workout;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class WorkoutListFragment extends ListFragment {
    private static final String STATE_ACTIVATED_POSITION = "activated_position";
    private static Callbacks itemSelectedCallback = new Callbacks(){

        @Override
        public void onItemSelected(String string) {
        }

        @Override
        public void updateAdapter() {
        }
    };

    private SimpleAdapter adapter;
    String[] from;
    private int mActivatedPosition = -1;
    private Callbacks mCallbacks = WorkoutListFragment.itemSelectedCallback;
    int[] to;
    private Workout workout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.workout = new Workout(getActivity());
        this.from = new String[]{"workoutName", "lastWorkout"};
        this.to = new int[]{R.id.workoutName, R.id.lastWorkout};
        this.adapter = new SimpleAdapter(getActivity(), generateMap(), R.layout.workout_list_item, this.from, this.to);
        this.setListAdapter(this.adapter);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState != null
                && savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
            setActivatedPosition(savedInstanceState.getInt(STATE_ACTIVATED_POSITION));
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (!(activity instanceof Callbacks)) {
            throw new IllegalStateException("Activity must implement fragment's callbacks.");
        }

        mCallbacks = (Callbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.mCallbacks = WorkoutListFragment.itemSelectedCallback;
    }

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        super.onListItemClick(listView, view, position, id);

        // Notify the active callbacks interface (the activity, if the
        // fragment is attached to one) that an item has been selected.
        mCallbacks.onItemSelected(this.workout.workouts.get(position).workoutID);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mActivatedPosition != ListView.INVALID_POSITION) {
            // Serialize and persist the activated item position.
            outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        this.updateAdapter();
    }

    public void setActivateOnItemClick(boolean activateOnItemClick) {
        // When setting CHOICE_MODE_SINGLE, ListView will automatically
        // give items the 'activated' state when touched.
        getListView().setChoiceMode(activateOnItemClick
                ? ListView.CHOICE_MODE_SINGLE
                : ListView.CHOICE_MODE_NONE);
    }


    private void setActivatedPosition(int position) {
        if (position == ListView.INVALID_POSITION) {
            getListView().setItemChecked(mActivatedPosition, false);
        } else {
            getListView().setItemChecked(position, true);
        }

        mActivatedPosition = position;
    }

    public List<HashMap<String, String>> generateMap() {
        List<HashMap<String, String>> workoutList = new ArrayList<HashMap<String, String>>();
        for (Workout.WorkoutItem workoutItem : this.workout.workouts) {

            HashMap<String, String> workoutMap = new HashMap<String, String>();
            workoutMap.put("workoutName", workoutItem.workoutName);
            long workoutTime = (1000 * workoutItem.lastWorkout);
            String workoutTimeStr = (workoutTime > 0) ?
                    (DateFormat.getDateFormat(getActivity()).format(new Date(workoutTime))) :
                    (getResources().getString(R.string.none));
            workoutMap.put("lastWorkout", workoutTimeStr);
            workoutList.add(workoutMap);
        }
        return workoutList;
    }


    public void updateAdapter() {
        this.workout = new Workout(getActivity());
        this.adapter = new SimpleAdapter(getActivity(), generateMap(), R.layout.workout_list_item, from, to);
        this.setListAdapter(this.adapter);
    }

    public interface Callbacks {
        public void onItemSelected(String var1);

        public void updateAdapter();
    }

}

