package com.joshafeinberg.workouttracker;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.joshafeinberg.workouttracker.database.DatabaseHelper;
import com.joshafeinberg.workouttracker.libraries.EditWorkoutListAdapter;
import com.joshafeinberg.workouttracker.libraries.WorkoutExerciseListAdapter;
import com.joshafeinberg.workouttracker.models.Exercise;
import com.joshafeinberg.workouttracker.models.Workout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WorkoutDetailFragment extends Fragment implements NewWorkoutFragment.FragmentCallbacks, EditWorkoutListAdapter.Callback {
    public static final String ARG_WORKOUT_ID = "workout_id";
    private List<Integer> allWeights;
    private EditWorkoutListAdapter editWorkoutListAdapter;
    private Exercise exerciseClass;
    private ListView exerciseTable;
    private boolean inEditMode;
    private NewWorkoutFragment.Callbacks mCallbacks;
    private Workout.WorkoutItem mItem;
    private Menu menu;
    private View rootView;
    private int selectedWeight;
    private List<Exercise.ExerciseItem> unusedExercises;
    private Spinner weightSpinner;
    private ArrayAdapter<String> weightsAdapter;
    private WorkoutExerciseListAdapter workoutExerciseListAdapter;

    public WorkoutDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().containsKey(ARG_WORKOUT_ID)) {
            this.mItem = new DatabaseHelper(getActivity()).getWorkout(getArguments().getString(ARG_WORKOUT_ID));
            if (getActivity().getActionBar() != null) {
                getActivity().getActionBar().setTitle(this.mItem.workoutName);
            }
        }
        this.setRetainInstance(true);
        this.setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (mItem != null) {
            if (inEditMode) {
                editWorkout();
            }
            updateAdapter();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (inEditMode) {
            finishEditWorkout();
        }
        this.mCallbacks = null;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        if (inEditMode) {
            hideOptionsMenu(true);
        }
        this.menu = menu;
    }

    @SuppressLint("NewApi")
    public void hideOptionsMenu(boolean hide) {
        for (int i = 0; i < menu.size(); ++i) {
            MenuItem menuItem = menu.getItem(i);
            menuItem.setVisible(!hide);
        }

        if (getActivity().getActionBar() != null) {

            if (android.os.Build.VERSION.SDK_INT > 14) {
                getActivity().getActionBar().setHomeButtonEnabled(!hide);
            }
            getActivity().getActionBar().setDisplayShowHomeEnabled(!hide);
            getActivity().getActionBar().setDisplayHomeAsUpEnabled(!hide);
            getActivity().getActionBar().setDisplayShowTitleEnabled(!hide);
            getActivity().getActionBar().setDisplayShowCustomEnabled(hide);

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (selectedWeight != 0) {
            this.allWeights = new DatabaseHelper(getActivity()).getAllWeightsByWorkout(this.mItem.workoutID);
            this.weightsAdapter.clear();
            String weightMeasurement = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("pref_weightMeasurement", "lbs");
            ArrayList<String> allWeightsList = new ArrayList<String>();
            for (Integer allWeight : this.allWeights) {
                allWeightsList.add(allWeight + " " + weightMeasurement);
            }
            this.weightsAdapter.addAll(allWeightsList);
            this.weightsAdapter.notifyDataSetChanged();
            int n = this.allWeights.indexOf(selectedWeight);
            this.weightSpinner.setSelection(n);
        }
    }



    public void addExercise() {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View view = inflater.inflate(R.layout.add_exercise, null);
        if (view == null) {
            return;
        }

        ArrayList<String> usedExercises = new ArrayList<String>();
        for (Exercise.ExerciseItem exerciseItem : this.exerciseClass.exercises) {
            usedExercises.add(exerciseItem.exerciseID);
        }

        final DatabaseHelper databaseHelper = new DatabaseHelper(getActivity());
        final Spinner newWorkoutSpinner = (Spinner) view.findViewById(R.id.newWorkoutAddExerciseSpinner);
        final List<Exercise.ExerciseItem> allExercises = databaseHelper.getAllExercises();
        this.unusedExercises = new ArrayList<Exercise.ExerciseItem>(allExercises);
        List<Map<String, String>> spinnerItems = new ArrayList<Map<String, String>>();

        HashMap<String, String> newItem = new HashMap<String, String>();
        newItem.put("exerciseName", getString(R.string.newworkoutaddnew));
        spinnerItems.add(newItem);
        for (Exercise.ExerciseItem exerciseItem : allExercises) {
            if (usedExercises.indexOf(exerciseItem.exerciseID) == -1) {
                HashMap<String, String> spinnerItem = new HashMap<String, String>();
                spinnerItem.put("exerciseName", exerciseItem.exerciseName);
                spinnerItems.add(spinnerItem);
            } else {
                unusedExercises.remove(exerciseItem);
            }
        }

        String[] from = new String[]{"exerciseName"};
        int[] to = new int[]{android.R.id.text1};
        newWorkoutSpinner.setAdapter(new SimpleAdapter(getActivity(), spinnerItems, android.R.layout.simple_spinner_item, from, to));

        final EditText newWorkoutExerciseName = (EditText) view.findViewById(R.id.newWorkoutAddExerciseText);
        newWorkoutSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    newWorkoutExerciseName.setVisibility(View.GONE);
                } else {
                    newWorkoutExerciseName.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.newworkoutaddexercise);
        builder.setView(view);
        builder.setPositiveButton(R.string.newworkoutaddexercise, null);
        builder.setNegativeButton(android.R.string.cancel, null);
        final AlertDialog dialog = builder.create();
        dialog.show();
        Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        if (positiveButton != null) {
            positiveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int selected = newWorkoutSpinner.getSelectedItemPosition();
                    if (newWorkoutExerciseName.getText() != null) {
                        if (newWorkoutExerciseName.getText().toString().equals("") && selected == 0) {
                            Toast.makeText(getActivity(), R.string.exerciseerror, Toast.LENGTH_LONG).show();
                            return;
                        }
                    }
                    Exercise.ExerciseItem exerciseItem;
                    if (selected == 0) {
                        String newExerciseName = newWorkoutExerciseName.getText().toString();
                        long newExercise = databaseHelper.addExercise(newExerciseName, "");
                        exerciseItem = new Exercise.ExerciseItem(Long.toString(newExercise), newExerciseName, 0, 0);
                    } else {
                        exerciseItem = unusedExercises.get(selected - 1);
                    }

                    Exercise.ExerciseItem addButton = editWorkoutListAdapter.getItem(editWorkoutListAdapter.getCount() - 1);
                    editWorkoutListAdapter.remove(addButton);
                    editWorkoutListAdapter.add(exerciseItem);
                    editWorkoutListAdapter.add(addButton);
                    editWorkoutListAdapter.notifyDataSetChanged();
                    databaseHelper.addWorkoutExercise(mItem.workoutID, exerciseItem.exerciseID);
                    WorkoutDetailFragment.this.rootView.findViewById(R.id.editExerciseHeaderTable).setVisibility(View.VISIBLE);
                    WorkoutDetailFragment.this.rootView.findViewById(R.id.editExerciseHeaderTable).setVisibility(View.GONE);
                    dialog.dismiss();
                }
            });

        }

    }

    public void editWorkout() {
        this.inEditMode = true;
        if (getActivity().getActionBar() != null) {
            this.hideOptionsMenu(true);
            View newActionBar = LayoutInflater.from(getActivity()).inflate(R.layout.edit_workout_action_bar, null);
            if (newActionBar != null) {
                TextView editWorkoutDone = (TextView) newActionBar.findViewById(R.id.editWorkoutDone);
                editWorkoutDone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finishEditWorkout();
                    }
                });
                getActivity().getActionBar().setCustomView(newActionBar);
            }
        }

        this.editWorkoutListAdapter = new EditWorkoutListAdapter(getActivity(), R.layout.new_workout_exercise_list_item, this.exerciseClass.exercises);

        this.exerciseTable.setSelector(android.R.color.transparent);

        this.editWorkoutListAdapter.setCallback(this);
        this.editWorkoutListAdapter.add(new Exercise.ExerciseItem("ADD", "ADD", 0, 0));
        this.exerciseTable.setAdapter(this.editWorkoutListAdapter);
        Button newWorkout = (Button)(this.rootView.findViewById(R.id.newWorkout));
        RelativeLayout weightSelector = (RelativeLayout)(this.rootView.findViewById(R.id.weightSelectionLayout));
        TableLayout exerciseHeader = (TableLayout)(this.rootView.findViewById(R.id.exerciseHeaderTable));
        TableLayout editExerciseHeader = (TableLayout)(this.rootView.findViewById(R.id.editExerciseHeaderTable));
        newWorkout.setVisibility(View.GONE);
        weightSelector.setVisibility(View.GONE);
        exerciseHeader.setVisibility(View.INVISIBLE);
        editExerciseHeader.setVisibility(View.VISIBLE);
    }

    public void finishEditWorkout() {
        hideOptionsMenu(false);
        getActivity().onPrepareOptionsMenu(menu);
        inEditMode = false;

        if (editWorkoutListAdapter.getCount() > 1)
        {
            editWorkoutListAdapter.remove(workoutExerciseListAdapter.getItem(workoutExerciseListAdapter.getCount() - 1));
        }

        exerciseTable.setSelector(android.R.color.transparent);

        exerciseTable.setAdapter(workoutExerciseListAdapter);
        Button newWorkout = (Button) this.rootView.findViewById(R.id.newWorkout);
        RelativeLayout weightSelectionLayout = (RelativeLayout) this.rootView.findViewById(R.id.weightSelectionLayout);
        TableLayout exerciseHeaderTable = (TableLayout) this.rootView.findViewById(R.id.exerciseHeaderTable);
        TableLayout editExerciseHeaderTable = (TableLayout) this.rootView.findViewById(R.id.editExerciseHeaderTable);
        newWorkout.setVisibility(View.VISIBLE);
        weightSelectionLayout.setVisibility(View.VISIBLE);
        exerciseHeaderTable.setVisibility(View.VISIBLE);
        editExerciseHeaderTable.setVisibility(View.GONE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        this.rootView = inflater.inflate(R.layout.fragment_workout_detail, container, false);

        if (this.rootView == null) {
            return null;
        }

        final String weightMeasurement = PreferenceManager.getDefaultSharedPreferences(getActivity())
                                        .getString("pref_weightMeasurement", "lbs");
        Button newWorkout = (Button) rootView.findViewById(R.id.newWorkout);
        newWorkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mItem.workoutType == Workout.workoutTypes.REPS) {
                    View numberPicker = LayoutInflater.from(getActivity()).inflate(R.layout.number_picker_widget, null);
                    if (numberPicker != null) {
                        Button minusButton = (Button) numberPicker.findViewById(R.id.newWorkoutWeightMinus);
                        final EditText workoutWeight = (EditText) numberPicker.findViewById(R.id.newWorkoutWeight);
                        TextView workoutMeasurement = (TextView) numberPicker.findViewById(R.id.newWorkoutMeasurement);
                        workoutMeasurement.setText(weightMeasurement);
                        Button plusButton = (Button) numberPicker.findViewById(R.id.newWorkoutWeightPlus);

                        minusButton.setOnClickListener(new View.OnClickListener(){
                            public void onClick(View view) {
                                if (workoutWeight.getText() != null) {
                                    String weightValue = workoutWeight.getText().toString();
                                    int weightValueInt;
                                    if (weightValue.equals("")) {
                                        weightValue = "5";
                                    }
                                    weightValueInt = Integer.valueOf(weightValue);
                                    weightValueInt -= 5;
                                    if (weightValueInt <= 0) {
                                        weightValueInt = 5;
                                    }
                                    workoutWeight.setText(Integer.toString(weightValueInt));
                                }
                            }
                        });

                        plusButton.setOnClickListener(new View.OnClickListener(){

                            public void onClick(View view) {
                                if (workoutWeight.getText() != null) {
                                    String weightValue = workoutWeight.getText().toString();
                                    int weightValueInt;
                                    if (weightValue.equals("")) {
                                        weightValue = "5";
                                    }
                                    weightValueInt = Integer.valueOf(weightValue);
                                    weightValueInt += 5;
                                    workoutWeight.setText(Integer.toString(weightValueInt));
                                }
                            }
                        });

                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setTitle(R.string.newworkoutaddexercise);
                        builder.setView(numberPicker);
                        builder.setPositiveButton(R.string.startworkout, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (workoutWeight.getText() != null) {
                                    String weightValue = workoutWeight.getText().toString();
                                    startWorkout(Integer.valueOf(weightValue));
                                }
                            }
                        });
                        builder.show();
                    }

                } else {
                    startWorkout(0);
                }
            }
        });

        final DatabaseHelper databaseHelper = new DatabaseHelper(getActivity());
        final TextView exerciseTop = (TextView) rootView.findViewById(R.id.exerciseTop);
        RelativeLayout weightSelectionLayout = (RelativeLayout) rootView.findViewById(R.id.weightSelectionLayout);
        switch(mItem.workoutType) {
            case REPS:
                exerciseTop.setText(R.string.maxreps);
                weightSelectionLayout.setVisibility(View.VISIBLE);
                weightSpinner = (Spinner) rootView.findViewById(R.id.weightSpinner);
                allWeights = databaseHelper.getAllWeightsByWorkout(mItem.workoutID);
                ArrayList<String> myWeights = new ArrayList<String>();
                for (int weight : allWeights) {
                    myWeights.add(weight + " " + weightMeasurement);
                }
                weightsAdapter = new ArrayAdapter<String>(getActivity(), R.layout.workout_weight_list_item, myWeights);
                weightSpinner.setAdapter(weightsAdapter);
                weightSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        selectedWeight = Integer.valueOf(weightsAdapter.getItem(position).split(" [A-Za-z]+")[0]);
                        updateAdapter();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
                break;
            case MAX:
                exerciseTop.setText(R.string.maxweight);
                weightSelectionLayout.setVisibility(View.GONE);
                selectedWeight = 0;
                break;
        }

        this.updateAdapter();
        this.exerciseTable = (ListView) rootView.findViewById(R.id.exerciseTable);
        this.exerciseTable.setSelector(android.R.color.transparent);
        this.exerciseTable.setAdapter(this.workoutExerciseListAdapter);

        return this.rootView;
    }




    @Override
    public void removeExercise(Exercise.ExerciseItem exerciseItem) {
        final DatabaseHelper databaseHelper = new DatabaseHelper(getActivity());
        databaseHelper.deleteWorkoutExercise(this.mItem.workoutID, exerciseItem.exerciseID);
        this.updateAdapter();
        this.editWorkoutListAdapter.remove(exerciseItem);
        if (editWorkoutListAdapter.getItem(editWorkoutListAdapter.getCount() - 1) != null) {
            if (editWorkoutListAdapter.getItem(editWorkoutListAdapter.getCount() - 1).exerciseID.equals("ADD")) {
                editWorkoutListAdapter.add(new Exercise.ExerciseItem("ADD", "ADD", 0, 0));
            }
        }
        this.editWorkoutListAdapter.notifyDataSetChanged();
        this.rootView.findViewById(R.id.editExerciseHeaderTable).setVisibility(View.VISIBLE);
        this.rootView.findViewById(R.id.editExerciseHeaderTable).setVisibility(View.GONE);
        final Fragment mThis = this;
        if (editWorkoutListAdapter.getCount() == 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.deleteworkout);
            builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    databaseHelper.deleteWorkout(mItem.workoutID);
                    mCallbacks.updateAdapter();
                    finishEditWorkout();
                    getActivity().getSupportFragmentManager().beginTransaction().remove(mThis).commit();
                }
            });
            builder.setNegativeButton(android.R.string.cancel, null);
            builder.show();
        }
    }

    public void setCallback(NewWorkoutFragment.Callbacks callbacks) {
        this.mCallbacks = callbacks;
    }

    public void startWorkout(int selectedWeight) {
        Bundle bundle = new Bundle();
        bundle.putString(NewWorkoutFragment.ARG_WORKOUT_ID, this.mItem.workoutID);
        bundle.putInt(NewWorkoutFragment.ARG_WORKOUT_WEIGHT, selectedWeight);
        NewWorkoutFragment newWorkoutFragment = new NewWorkoutFragment();
        newWorkoutFragment.setArguments(bundle);
        newWorkoutFragment.setCallback(this.mCallbacks);
        newWorkoutFragment.setFragmentCallback(this);
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.workout_detail_container, newWorkoutFragment).addToBackStack(null).commit();
    }


    public void updateAdapter() {
        if (selectedWeight != 0 && mItem.workoutType == Workout.workoutTypes.REPS) {
            exerciseClass = new Exercise(getActivity(), mItem.workoutID, selectedWeight);
        } else {
            exerciseClass = new Exercise(getActivity(), mItem.workoutID);
        }
        if (this.workoutExerciseListAdapter == null) {
            this.workoutExerciseListAdapter = new WorkoutExerciseListAdapter(getActivity(), R.layout.exercise_list_item, this.exerciseClass.exercises, this.mItem.workoutType);
        } else {
            this.workoutExerciseListAdapter.clear();
            this.workoutExerciseListAdapter.addAll(this.exerciseClass.exercises);
            this.workoutExerciseListAdapter.notifyDataSetChanged();
        }
    }

    public void updateWeights(int selectedWeight) {
        this.selectedWeight = selectedWeight;
    }

}

