package com.joshafeinberg.workouttracker;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.joshafeinberg.workouttracker.database.DatabaseHelper;
import com.joshafeinberg.workouttracker.libraries.ManageExerciseListAdapter;
import com.joshafeinberg.workouttracker.models.Exercise;

import java.util.List;

public class ManageExercisesFragment extends Fragment implements ManageExerciseListAdapter.Callback {
    private ManageExerciseListAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getActivity() != null && getActivity().getActionBar() != null) {
            getActivity().getActionBar().setTitle(getString(R.string.manageexercises));
        }

    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View view = layoutInflater.inflate(R.layout.manage_exercises, viewGroup, false);

        if (view != null) {
            ListView exerciseList = (ListView) view.findViewById(R.id.exerciseList);
            List<Exercise.ExerciseItem> exerciseItemList = new DatabaseHelper(getActivity()).getAllExercises();
            this.adapter = new ManageExerciseListAdapter(getActivity(), R.layout.new_workout_exercise_list_item, exerciseItemList);
            this.adapter.setCallback(this);
            exerciseList.setAdapter(this.adapter);
        }

        return view;
    }

    @Override
    public void removeExercise(Exercise.ExerciseItem exerciseItem) {
        DatabaseHelper databaseHelper = new DatabaseHelper(getActivity());
        if (databaseHelper.findAllWorkoutsByExercise(exerciseItem.exerciseID) > 0) {
            Toast.makeText(getActivity(), R.string.deleteexerciseerror, Toast.LENGTH_LONG).show();
            return;
        }
        databaseHelper.deleteExercise(exerciseItem.exerciseID);
        this.adapter.remove(exerciseItem);
        this.adapter.notifyDataSetChanged();
    }
}

