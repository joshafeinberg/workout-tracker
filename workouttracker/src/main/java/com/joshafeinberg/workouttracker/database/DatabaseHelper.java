package com.joshafeinberg.workouttracker.database;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.preference.PreferenceManager;

import com.joshafeinberg.workouttracker.models.Exercise;
import com.joshafeinberg.workouttracker.models.Workout;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "workouttracker.db";
    private static final int DATABASE_VERSION = 1;
    private int weightType;
    public static final int POUNDS = 0;
    public static final int KILOGRAMS = 1;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        String string = PreferenceManager.getDefaultSharedPreferences(context).getString("pref_weightMeasurement", "lbs");
        if (string.equals("lbs")) {
            this.weightType = POUNDS;
        } else if (string.equals("kgs")) {
            this.weightType = KILOGRAMS;
        }
    }

    public void onCreate(SQLiteDatabase database) {
        WorkoutTable.onCreate(database);
        ExerciseTable.onCreate(database);
        WorkoutExercisesTable.onCreate(database);
        IndividualWorkoutTable.onCreate(database);
    }

    public void onOpen(SQLiteDatabase database) {
        super.onOpen(database);
        if (database.isReadOnly()) return;
        database.execSQL("PRAGMA foreign_keys=ON;");
    }

    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        WorkoutTable.onUpgrade(database, oldVersion, newVersion);
        ExerciseTable.onUpgrade(database, oldVersion, newVersion);
        WorkoutExercisesTable.onUpgrade(database, oldVersion, newVersion);
        IndividualWorkoutTable.onUpgrade(database, oldVersion, newVersion);
    }



    public static int kilogramsToPounds(long lbs) {
        return (int) Math.floor(2.2046 * lbs);
    }

    public static int poundsToKilograms(long kgs) {
        return (int) Math.floor(kgs / 2.2046);
    }

    public long addExercise(String exerciseName, String workoutID) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ExerciseTable.COLUMN_NAME, exerciseName);
        long row = -1;

        if (database != null) {
            row = database.insert(ExerciseTable.TABLE_NAME, null, contentValues);
            if (!(workoutID.equals(""))) {
               this.addWorkoutExercise(workoutID, Long.toString(row));
            }
            database.close();
        }

        return row;
    }

    public long addIndividualWorkout(String workoutID, String exerciseID, int reps, int weight, long time) {
        SQLiteDatabase database = this.getWritableDatabase();
        if (this.weightType == KILOGRAMS) {
            weight = DatabaseHelper.kilogramsToPounds(weight);
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put(IndividualWorkoutTable.COLUMN_WORKOUT_ID, workoutID);
        contentValues.put(IndividualWorkoutTable.COLUMN_EXERCISE_ID, exerciseID);
        contentValues.put(IndividualWorkoutTable.COLUMN_WORKOUT_REPS, reps);
        contentValues.put(IndividualWorkoutTable.COLUMN_WORKOUT_WEIGHT, weight);
        contentValues.put(IndividualWorkoutTable.COLUMN_WORKOUT_TIME, time);
        long row = -1;
        if (database != null) {
            row = database.insert(IndividualWorkoutTable.TABLE_NAME, null, contentValues);
            database.close();
        }
        return row;
    }

    public long addWorkout(String workoutName, Workout.workoutTypes workoutType) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(WorkoutTable.COLUMN_NAME, workoutName);
        contentValues.put(WorkoutTable.COLUMN_TYPE, workoutType.ordinal());
        long row = -1;
        if (database != null) {
            row = database.insert(WorkoutTable.TABLE_NAME, null, contentValues);
            database.close();
        }
        return row;
    }

    public void addWorkoutExercise(String workoutID, String exerciseID) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(WorkoutExercisesTable.COLUMN_WORKOUT_ID, workoutID);
        contentValues.put(WorkoutExercisesTable.COLUMN_EXERCISE_ID, exerciseID);
        if (database != null) {
            database.insert(WorkoutExercisesTable.TABLE_NAME, null, contentValues);
            database.close();
        }
    }

    public void deleteExercise(String exerciseID) {
        SQLiteDatabase database = this.getWritableDatabase();
        if (database != null) {
            database.delete(ExerciseTable.TABLE_NAME, ExerciseTable.COLUMN_ID + " = ?", new String[]{exerciseID});
            database.close();
        }
    }

    public void deleteWorkout(String workoutID) {
        SQLiteDatabase database = this.getWritableDatabase();
        if (database != null) {
            database.delete(WorkoutTable.TABLE_NAME, WorkoutTable.COLUMN_ID + " = ?", new String[]{workoutID});
            database.close();
        }
    }

    public void deleteWorkoutExercise(String workoutID, String exerciseID) {
        SQLiteDatabase database = this.getWritableDatabase();
        if (database != null) {
            database.delete(WorkoutExercisesTable.TABLE_NAME,
                    WorkoutExercisesTable.COLUMN_WORKOUT_ID + " = ? AND " +
                    WorkoutExercisesTable.COLUMN_EXERCISE_ID + " = ?", new String[]{workoutID, exerciseID});
            database.close();
        }
    }

    public int findAllWorkoutsByExercise(String exerciseID) {
        SQLiteDatabase database = this.getWritableDatabase();
        String selectQuery = ("SELECT * FROM " + WorkoutExercisesTable.TABLE_NAME +
                " WHERE " + WorkoutExercisesTable.COLUMN_EXERCISE_ID + "=" + exerciseID);
        int row = -1;
        if (database != null) {
            row = database.rawQuery(selectQuery, null).getCount();
            database.close();
        }
        return row;
    }

    public List<Exercise.ExerciseItem> getAllExercises() {
        Cursor cursor;
        List<Exercise.ExerciseItem> exerciseItemList = new ArrayList<Exercise.ExerciseItem>();
        SQLiteDatabase database = this.getWritableDatabase();
        if (database != null) {
            cursor = database.rawQuery("SELECT * FROM " + ExerciseTable.TABLE_NAME, null);
            if (cursor.moveToFirst()) {
                do {
                    exerciseItemList.add(new Exercise.ExerciseItem(cursor.getString(0), cursor.getString(1), 0, 0));
                } while (cursor.moveToNext());
            }
        }
        return exerciseItemList;
    }

    public List<Exercise.ExerciseItem> getAllExercises(String workoutID) {
        Cursor cursor;
        List<Exercise.ExerciseItem> exerciseItemList = new ArrayList<Exercise.ExerciseItem>();
        String queryString = ("SELECT E.*, MAX(IW." + IndividualWorkoutTable.COLUMN_WORKOUT_REPS + "), " +
                                "MAX(IW." + IndividualWorkoutTable.COLUMN_WORKOUT_WEIGHT + ") " +
                                "FROM " + ExerciseTable.TABLE_NAME + " E " +
                                "INNER JOIN " + WorkoutExercisesTable.TABLE_NAME + " WE " +
                                    "ON WE." + WorkoutExercisesTable.COLUMN_EXERCISE_ID + "=E." + ExerciseTable.COLUMN_ID + " " +
                                "LEFT JOIN (SELECT * FROM " + IndividualWorkoutTable.TABLE_NAME + " " +
                                    "WHERE " + IndividualWorkoutTable.COLUMN_WORKOUT_ID + "=" + workoutID + ") IW" + " " +
                                        " ON IW." + IndividualWorkoutTable.COLUMN_EXERCISE_ID + "=" + "WE." + WorkoutExercisesTable.COLUMN_EXERCISE_ID + " " +
                                "WHERE WE." + WorkoutExercisesTable.COLUMN_WORKOUT_ID + "=" + workoutID + " GROUP BY E." + ExerciseTable.COLUMN_ID);
        SQLiteDatabase database = this.getWritableDatabase();
        if (database != null) {
            cursor = database.rawQuery(queryString, null);
            if (cursor.moveToFirst()) {
                do {
                    int weight = cursor.getInt(3);
                    if (this.weightType == KILOGRAMS) {
                        weight = DatabaseHelper.poundsToKilograms(weight);
                    }
                    exerciseItemList.add(new Exercise.ExerciseItem(cursor.getString(0), cursor.getString(1), cursor.getInt(2), weight));
                } while (cursor.moveToNext());
            }
        }
        return exerciseItemList;
    }

    public List<Exercise.ExerciseItem> getAllExercisesByWeight(String workoutID, int weight) {
        Cursor cursor;
        List<Exercise.ExerciseItem> exerciseItemList = new ArrayList<Exercise.ExerciseItem>();
        String queryString = ("SELECT E.*, MAX(IW." + IndividualWorkoutTable.COLUMN_WORKOUT_REPS + ") " +
                "FROM " + ExerciseTable.TABLE_NAME + " E " +
                "INNER JOIN " + WorkoutExercisesTable.TABLE_NAME + " WE " +
                "ON WE." + WorkoutExercisesTable.COLUMN_EXERCISE_ID + "=E." + ExerciseTable.COLUMN_ID + " " +
                "LEFT JOIN (SELECT * FROM " + IndividualWorkoutTable.TABLE_NAME + " " +
                "WHERE " + IndividualWorkoutTable.COLUMN_WORKOUT_ID + "=" + workoutID + " AND " +
                    IndividualWorkoutTable.COLUMN_WORKOUT_WEIGHT + "=" + weight + ") IW" + " " +
                " ON IW." + IndividualWorkoutTable.COLUMN_EXERCISE_ID + "=" + "WE." + WorkoutExercisesTable.COLUMN_EXERCISE_ID + " " +
                "WHERE WE." + WorkoutExercisesTable.COLUMN_WORKOUT_ID + "=" + workoutID + " GROUP BY E." + ExerciseTable.COLUMN_ID);
        SQLiteDatabase database = this.getWritableDatabase();
        if (database != null) {
            cursor = database.rawQuery(queryString, null);
            if (cursor.moveToFirst()) {
                do {
                    if (this.weightType == KILOGRAMS) {
                        weight = DatabaseHelper.poundsToKilograms(weight);
                    }
                    exerciseItemList.add(new Exercise.ExerciseItem(cursor.getString(0), cursor.getString(1), cursor.getInt(2), weight));
                } while (cursor.moveToNext());
            }
        }
        return exerciseItemList;
    }


    public List<Integer> getAllWeightsByWorkout(String workoutID) {
        List<Integer> integerList = new ArrayList<Integer>();
        String queryString = ("SELECT DISTINCT " + IndividualWorkoutTable.COLUMN_WORKOUT_WEIGHT + " " +
                                "FROM " + IndividualWorkoutTable.TABLE_NAME + " " +
                                "WHERE " +IndividualWorkoutTable.COLUMN_WORKOUT_REPS + " != 0 AND " + IndividualWorkoutTable.COLUMN_WORKOUT_ID + "=" + workoutID);
        SQLiteDatabase database = this.getWritableDatabase();
        if (database != null) {
            Cursor cursor = database.rawQuery(queryString, null);
            if (cursor.moveToFirst()) {
                do {
                    if (this.weightType == POUNDS) {
                        integerList.add(cursor.getInt(0));
                    } else if (this.weightType == KILOGRAMS) {
                        integerList.add(poundsToKilograms(cursor.getInt(0)));
                    }
                } while (cursor.moveToNext());
            }
        }
        return integerList;
    }

    public List<Workout.WorkoutItem> getAllWorkouts() {
        List<Workout.WorkoutItem> workoutItemList = new ArrayList<Workout.WorkoutItem>();
        String queryString = "SELECT W.*, MAX(IW." + IndividualWorkoutTable.COLUMN_WORKOUT_TIME + ") " +
                             "FROM " + WorkoutTable.TABLE_NAME + " W " +
                             "LEFT JOIN " + IndividualWorkoutTable.TABLE_NAME + " IW ON " +
                                "IW." + IndividualWorkoutTable.COLUMN_WORKOUT_ID + "=W." + WorkoutTable.COLUMN_ID + " " +
                             "GROUP BY W." + WorkoutTable.COLUMN_ID;
        SQLiteDatabase database = this.getWritableDatabase();
        if (database != null) {
            Cursor cursor = database.rawQuery(queryString, null);

            if (cursor.moveToFirst()) {
                do {
                    workoutItemList.add(new Workout.WorkoutItem(cursor.getInt(0), cursor.getString(1), cursor.getInt(2), cursor.getLong(3)));
                } while (cursor.moveToNext());
            }
        }
        return workoutItemList;
    }

    public Workout.WorkoutItem getWorkout(String workoutID) {
        Workout.WorkoutItem workoutItem = null;
        String queryString = "SELECT W.*, MAX(IW." + IndividualWorkoutTable.COLUMN_WORKOUT_TIME + ") " +
                             "FROM " + WorkoutTable.TABLE_NAME + " W " +
                             "LEFT JOIN " + IndividualWorkoutTable.TABLE_NAME + " IW " +
                                "ON IW." + IndividualWorkoutTable.COLUMN_WORKOUT_ID + "=W." + WorkoutTable.COLUMN_ID + " " +
                             "WHERE W." + WorkoutTable.COLUMN_ID + "=" + workoutID + " GROUP BY W." + WorkoutTable.COLUMN_ID;
        SQLiteDatabase database = this.getReadableDatabase();

        if (database != null) {
            Cursor cursor = database.rawQuery(queryString, null);
            if (cursor.moveToFirst()) {
                workoutItem = new Workout.WorkoutItem(cursor.getInt(0), cursor.getString(1), cursor.getInt(2), cursor.getLong(3));
            }
        }
        return workoutItem;
    }

}

