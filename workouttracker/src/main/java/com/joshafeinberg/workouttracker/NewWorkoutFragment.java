package com.joshafeinberg.workouttracker;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.joshafeinberg.workouttracker.database.DatabaseHelper;
import com.joshafeinberg.workouttracker.models.Exercise;
import com.joshafeinberg.workouttracker.models.Workout;

import java.util.Calendar;
import java.util.List;

public class NewWorkoutFragment extends Fragment {
    public static final String ARG_WORKOUT_ID = "workout_id";
    public static final String ARG_WORKOUT_WEIGHT = "workout_weight";
    private static FragmentCallbacks fragmentCallback;
    private static Callbacks workoutAddedCallback;
    private SparseIntArray currentWorkoutValues;
    private DatabaseHelper dbHelper;
    private List<Exercise.ExerciseItem> exerciseItems;
    private FragmentCallbacks fragmentCallbacks = NewWorkoutFragment.fragmentCallback;
    private int lastSelected;
    private Callbacks mCallbacks = NewWorkoutFragment.workoutAddedCallback;
    private Workout.WorkoutItem mItem;
    private String workoutID;
    private int workoutWeight;

    static {
        NewWorkoutFragment.workoutAddedCallback = new Callbacks(){

            @Override
            public void updateAdapter() {
            }
        };
        NewWorkoutFragment.fragmentCallback = new FragmentCallbacks(){

            @Override
            public void updateWeights(int n) {
            }
        };
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.dbHelper = new DatabaseHelper(getActivity());
        if (this.getArguments().containsKey(ARG_WORKOUT_ID)) {
            this.workoutID = this.getArguments().getString(ARG_WORKOUT_ID);
            this.mItem = this.dbHelper.getWorkout(this.workoutID);
        }
        if (this.getArguments().containsKey(ARG_WORKOUT_WEIGHT)) {
            this.workoutWeight = this.getArguments().getInt(ARG_WORKOUT_WEIGHT);
        }
        this.currentWorkoutValues = new SparseIntArray();
        this.lastSelected = 0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.new_workout, container, false);
        if (view != null) {
            final Spinner workoutSpinner = (Spinner) view.findViewById(R.id.workoutSpinner);
            final TextView workoutTypeAmount = (TextView) view.findViewById(R.id.newWorkoutTypeAmount);
            workoutTypeAmount.setText("");
            exerciseItems = dbHelper.getAllExercisesByWeight(workoutID, workoutWeight);
            workoutSpinner.setAdapter(new ArrayAdapter<Exercise.ExerciseItem>(getActivity(), android.R.layout.simple_spinner_item, exerciseItems));
            workoutSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (!(workoutTypeAmount.getText() == null || workoutTypeAmount.getText().toString().equals(""))) {
                        currentWorkoutValues.put(lastSelected, Integer.valueOf(workoutTypeAmount.getText().toString()));
                    }
                    workoutTypeAmount.setText(Integer.toString(currentWorkoutValues.get(position, 0)));
                    lastSelected = position;
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            TextView workoutTypeLabel = (TextView) view.findViewById(R.id.newWorkoutTypeLabel);
            switch (mItem.workoutType) {
                case REPS:
                    workoutTypeLabel.setText(R.string.newworkoutnumreps);
                    break;
                case MAX:
                    workoutTypeLabel.setText(R.string.newworkoutnumweight);
                    String workoutMeasurementStr = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("pref_weightMeasurement", "lbs");
                    TextView workoutMeasurement = (TextView) view.findViewById(R.id.newWorkoutMeasurement);
                    workoutMeasurement.setText(" " + workoutMeasurementStr);
                    break;
            }

            int[] numberButtons = new int[]{R.id.button1, R.id.button2, R.id.button3,
                    R.id.button4, R.id.button5, R.id.button6,
                    R.id.button7, R.id.button8, R.id.button9, R.id.button0};
            for (int i = 0; i < numberButtons.length; ++i) {
                final Button numButton = (Button) view.findViewById(numberButtons[i]);
                numButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (workoutTypeAmount.getText() != null && workoutTypeAmount.getText().equals("0")) {
                            workoutTypeAmount.setText("");
                        }
                        workoutTypeAmount.setText(workoutTypeAmount.getText() + "" + numButton.getText());
                    }
                });
            }

            Button backButton = (Button) view.findViewById(R.id.backButton);
            backButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (workoutTypeAmount.getText() != null) {
                        CharSequence amount = workoutTypeAmount.getText();
                        if (amount.length() >= 1) {
                            workoutTypeAmount.setText(amount.subSequence(0, (amount.length() - 1)));
                        } else if (amount.length() == 0) {
                            workoutTypeAmount.setText("0");
                        }

                    }
                }
            });

            Button nextButton = (Button) view.findViewById(R.id.nextButton);
            nextButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (!(workoutTypeAmount.getText() == null) || workoutTypeAmount.getText().equals("")) {
                        currentWorkoutValues.put(lastSelected, Integer.valueOf(workoutTypeAmount.getText().toString()));
                    }

                    if (exerciseItems.size() > workoutSpinner.getSelectedItemPosition() + 1) {
                        workoutSpinner.setSelection(workoutSpinner.getSelectedItemPosition() + 1);
                        workoutTypeAmount.setText(Integer.toString(currentWorkoutValues.get(workoutSpinner.getSelectedItemPosition() + 1)));
                        lastSelected = workoutSpinner.getSelectedItemPosition() + 1;
                    } else {
                        endWorkout();
                    }
                }
            });

            Button endWorkout = (Button) view.findViewById(R.id.endWorkout);
            endWorkout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!(workoutTypeAmount.getText() == null) || workoutTypeAmount.getText().equals("")) {
                        currentWorkoutValues.put(lastSelected, Integer.valueOf(workoutTypeAmount.getText().toString()));
                    }
                    endWorkout();
                }
            });
        }

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.mCallbacks = null;
        this.fragmentCallbacks = null;
    }

    private void endWorkout() {
        long currentTime = (Calendar.getInstance().getTimeInMillis() / 1000);
        for (int i = 0; i < this.exerciseItems.size(); ++i) {
            Exercise.ExerciseItem exerciseItem = exerciseItems.get(i);
            int value = currentWorkoutValues.get(i);
            if (value == 0) {
                continue;
            }
            int weight = 0;
            int reps = 0;
            boolean record = false;
            switch (mItem.workoutType) {
                case REPS:
                    weight = workoutWeight;
                    reps = value;
                    record = ((reps > exerciseItem.maxReps) || (exerciseItem.maxReps == 0)) && (exerciseItem.maxWeight == this.workoutWeight);
                    break;
                case MAX:
                    weight = value;
                    reps = 0;
                    record = (weight > exerciseItem.maxWeight) || (exerciseItem.maxWeight == 0);
                    break;
            }

            if (record) {
                Toast.makeText(getActivity(), getString(R.string.newrecord) + exerciseItem.exerciseName, Toast.LENGTH_SHORT).show();
            }

            dbHelper.addIndividualWorkout(workoutID, exerciseItem.exerciseID, reps, weight, currentTime);

        }

        if (mCallbacks != null) {
            mCallbacks.updateAdapter();
        }

        if (fragmentCallbacks != null) {
            fragmentCallbacks.updateWeights(workoutWeight);
        }

        getActivity().getSupportFragmentManager().popBackStack();
    }

    public void setCallback(Callbacks callbacks) {
        this.mCallbacks = callbacks;
    }

    public void setFragmentCallback(FragmentCallbacks fragmentCallbacks) {
        this.fragmentCallbacks = fragmentCallbacks;
    }

    public interface Callbacks {
        public void updateAdapter();
    }

    public interface FragmentCallbacks {
        public void updateWeights(int var1);
    }

}

