package com.joshafeinberg.workouttracker;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;

public class AddWorkoutActivity extends FragmentActivity implements AddWorkoutFragment.Callbacks {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_workout_detail);

        if (getActionBar() != null) {
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }

        AddWorkoutFragment addWorkoutFragment = (AddWorkoutFragment)(getSupportFragmentManager().findFragmentByTag("ADDWORKOUT"));

        if (savedInstanceState == null) {
            Bundle myBundle = new Bundle();
            if (getIntent().getStringExtra(WorkoutDetailFragment.ARG_WORKOUT_ID) != null) {
                myBundle.putString(WorkoutDetailFragment.ARG_WORKOUT_ID, getIntent().getStringExtra(WorkoutDetailFragment.ARG_WORKOUT_ID));
            }

            if (addWorkoutFragment == null) {
                addWorkoutFragment = new AddWorkoutFragment();
                addWorkoutFragment.setArguments(myBundle);
                addWorkoutFragment.setCallback(this);
                //getSupportFragmentManager().beginTransaction().add(R.id.workout_detail_container, addWorkoutFragment, "ADDWORKOUT").commit();
            }
        }


        getSupportFragmentManager().beginTransaction().add(R.id.workout_detail_container, addWorkoutFragment, "ADDWORKOUT").commit();
    }

    @Override
    public void onItemSelected(String string) {
        Intent intent = new Intent(this, WorkoutListActivity.class);
        intent.putExtra("workout_id", string);
        NavUtils.navigateUpTo(this, intent);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != android.R.id.home) {
            return super.onOptionsItemSelected(menuItem);
        }
        NavUtils.navigateUpTo(this, new Intent(this, WorkoutListActivity.class));
        return true;
    }

    public void updateAdapter() {
    }
}

