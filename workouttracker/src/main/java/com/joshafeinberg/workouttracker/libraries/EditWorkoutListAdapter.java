package com.joshafeinberg.workouttracker.libraries;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.joshafeinberg.workouttracker.R;
import com.joshafeinberg.workouttracker.models.Exercise;

import java.util.List;

public class EditWorkoutListAdapter extends ArrayAdapter<Exercise.ExerciseItem> {
    Callback callback;
    private int resource;

    public EditWorkoutListAdapter(Context context, int resource, List<Exercise.ExerciseItem> items) {
        super(context, resource, items);
        this.resource = resource;
    }

    public View getView(int row, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final Exercise.ExerciseItem exerciseItem = this.getItem(row);
        View rowView = view;

        if (exerciseItem.exerciseID.equals("ADD")) {
            View addExerciseRow = inflater.inflate(R.layout.new_workout_exercise_list_add_item, null);

            if (addExerciseRow != null) {
                addExerciseRow.findViewById(R.id.newWorkoutAddExercise).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callback.addExercise();
                    }
                });
            }

            return addExerciseRow;
        }

        if (view == null || view.findViewById(R.id.exerciseListItemName) == null) {
            rowView = inflater.inflate(this.resource, null);
        }

        if (rowView != null) {
            ((TextView)(rowView.findViewById(R.id.exerciseListItemName))).setText(exerciseItem.exerciseName);

            rowView.findViewById(R.id.exerciseListItemRemove).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callback.removeExercise(exerciseItem);
                }
            });
        }

        return rowView;
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public interface Callback {
        public void addExercise();

        public void removeExercise(Exercise.ExerciseItem exerciseItem);
    }

}

