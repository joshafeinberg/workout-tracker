
package com.joshafeinberg.workouttracker;


import android.app.AlertDialog;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.view.LayoutInflater;
import android.view.View;

public class WorkoutPreferencesFragment extends PreferenceFragment {

    public WorkoutPreferencesFragment() {
    }

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (getActivity() != null && getActivity().getActionBar() != null) {
            getActivity().getActionBar().setTitle(getString(R.string.preferences));
        }

        addPreferencesFromResource(R.xml.preferences);

        String weightPreference = "";
        if (getPreferenceScreen() != null && getPreferenceScreen().getSharedPreferences() != null) {
            weightPreference = getPreferenceScreen().getSharedPreferences().getString("pref_weightMeasurement", "lbs");
        }

        ListPreference weightMeasurement = (ListPreference)findPreference("pref_weightMeasurement");
        if (weightMeasurement != null)

        {
            final CharSequence weightMeasurementValues[] = weightMeasurement.getEntryValues();
            int i = findPreferencesPosition(weightMeasurementValues, weightPreference);
            final CharSequence weightMeasurements[] = weightMeasurement.getEntries();
            if (weightMeasurements != null)
            {
                weightMeasurement.setSummary(weightMeasurements[i]);
            }
            weightMeasurement.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {

                    int position = findPreferencesPosition(weightMeasurementValues, newValue.toString());
                    if (weightMeasurements != null) {
                        preference.setSummary(weightMeasurements[position]);
                    }

                    return true;
                }
            });
        }

        Preference aboutApp = findPreference("pref_aboutApp");
        String versionNumber = "1.0";
        if (getActivity() != null && getActivity().getPackageManager() != null) {
            try {
                versionNumber = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }


        if (aboutApp != null) {
            aboutApp.setSummary((new StringBuilder()).append("Version ").append(versionNumber).toString());
            aboutApp.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    View aboutView = LayoutInflater.from(getActivity()).inflate(R.layout.about_app, null);
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle(R.string.about);
                    builder.setView(aboutView);
                    builder.show();
                    return true;
                }
            });

        }
    }

    public int findPreferencesPosition(CharSequence[] weightPreferenceOptions, String weightPreference)
    {

        int i = 0;
        for (CharSequence option : weightPreferenceOptions) {
            if (option.equals(weightPreference)) {
                return i;
            }
            ++i;
        }

        return -1;
    }
}
