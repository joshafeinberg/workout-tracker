package com.joshafeinberg.workouttracker;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;

public class ManageExercisesActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_workout_detail);

        if (getActionBar() != null) {
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }


        if (savedInstanceState == null) {
            ManageExercisesFragment manageExercisesFragment = new ManageExercisesFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.workout_detail_container, manageExercisesFragment).commit();
        }
    }


    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != android.R.id.home) {
            return super.onOptionsItemSelected(menuItem);
        }
        NavUtils.navigateUpTo(this, new Intent(this, WorkoutListActivity.class));
        return true;
    }
}

