package com.joshafeinberg.workouttracker.libraries;

import android.content.Context;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.joshafeinberg.workouttracker.R;
import com.joshafeinberg.workouttracker.models.Exercise;
import com.joshafeinberg.workouttracker.models.Workout;

import java.util.List;

public class WorkoutExerciseListAdapter extends ArrayAdapter<Exercise.ExerciseItem> {
    private int resource;
    private Workout.workoutTypes workoutType;

    public WorkoutExerciseListAdapter(Context context, int resource, List<Exercise.ExerciseItem> items, Workout.workoutTypes workoutTypes) {
        super(context, resource, items);
        this.resource = resource;
        this.workoutType = workoutTypes;
    }


    public View getView(int row, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Exercise.ExerciseItem exerciseItem = this.getItem(row);
        View rowView = view;

        if (rowView == null) {
            rowView = inflater.inflate(this.resource, null);
        }

        if (rowView != null) {
            ((TextView)(rowView.findViewById(R.id.exerciseName))).setText(exerciseItem.exerciseName);

            String rowString = "";
            switch (this.workoutType) {
                case REPS:
                    rowString = Integer.toString(exerciseItem.maxReps);
                    break;
                case MAX:
                    String weightMeasurement = PreferenceManager.getDefaultSharedPreferences(this.getContext()).getString("pref_weightMeasurement", "lbs");
                    rowString = Integer.toString(exerciseItem.maxWeight) + " " + weightMeasurement;
                    break;

            }

            ((TextView)(rowView.findViewById(R.id.exerciseValue))).setText(rowString);
        }

        return rowView;
    }
}

