package com.joshafeinberg.workouttracker.models;

import android.content.Context;
import com.joshafeinberg.workouttracker.database.DatabaseHelper;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Exercise {
    public Map<String, ExerciseItem> exerciseMap;
    public List<ExerciseItem> exercises;

    public Exercise(Context context, String workoutID) {
        this.exercises = new DatabaseHelper(context).getAllExercises(workoutID);
        this.exerciseMap = new HashMap<String, ExerciseItem>();
        for (ExerciseItem exerciseItem : this.exercises) {
            this.exerciseMap.put(exerciseItem.exerciseID, exerciseItem);
        }
    }

    public Exercise(Context context, String workoutID, int weight) {
        this.exercises = new DatabaseHelper(context).getAllExercisesByWeight(workoutID, weight);
        this.exerciseMap = new HashMap<String, ExerciseItem>();
        for (ExerciseItem exerciseItem : this.exercises) {
            this.exerciseMap.put(exerciseItem.exerciseID, exerciseItem);
        }
    }

    public static class ExerciseItem {
        public String exerciseID;
        public String exerciseName;
        public int maxReps;
        public int maxWeight;

        public ExerciseItem(String exerciseID, String exerciseName, int maxReps, int maxWeight) {
            this.exerciseID = exerciseID;
            this.exerciseName = exerciseName;
            this.maxReps = maxReps;
            this.maxWeight = maxWeight;
        }

        public String toString() {
            return this.exerciseName;
        }
    }

}

