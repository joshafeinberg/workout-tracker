package com.joshafeinberg.workouttracker;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.joshafeinberg.workouttracker.database.DatabaseHelper;

public class WorkoutListActivity extends FragmentActivity implements WorkoutListFragment.Callbacks, AddWorkoutFragment.Callbacks, NewWorkoutFragment.Callbacks {
    private Fragment currentFragment;
    private boolean isItemSelected;
    private boolean isPreferencesSelected;
    private String itemSelected;
    private boolean mTwoPane;
    private WorkoutPreferencesFragment workoutPreferencesFragment;


    @Override
    protected void onCreate(Bundle onSavedState) {
        super.onCreate(onSavedState);
        setContentView(R.layout.activity_workout_list);

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        this.isItemSelected = false;
        this.isPreferencesSelected = false;

        if (findViewById(R.id.workout_detail_container) != null) {
            this.mTwoPane = true;
            ((WorkoutListFragment)this.getSupportFragmentManager().findFragmentById(R.id.workout_list)).setActivateOnItemClick(true);
        }

        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().containsKey(WorkoutDetailFragment.ARG_WORKOUT_ID)) {
                isItemSelected = true;
                onItemSelected(getIntent().getExtras().getString(WorkoutDetailFragment.ARG_WORKOUT_ID));
            }
        }
    }


    @Override
    public void onItemSelected(String id) {
        if (mTwoPane) {
            removePreferences();
            Bundle arguments = new Bundle();
            arguments.putString(WorkoutDetailFragment.ARG_WORKOUT_ID, id);
            WorkoutDetailFragment fragment = new WorkoutDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.workout_detail_container, fragment)
                    .commit();

        } else {
            // In single-pane mode, simply start the detail activity
            // for the selected item ID.
            Intent detailIntent = new Intent(this, WorkoutDetailActivity.class);
            detailIntent.putExtra(WorkoutDetailFragment.ARG_WORKOUT_ID, id);
            startActivity(detailIntent);
        }

        if (this.mTwoPane) {
            this.isItemSelected = true;
            this.itemSelected = id;
            this.invalidateOptionsMenu();
        }
        if (getActionBar() != null) {
            getActionBar().setTitle(R.id.appName);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.getMenuInflater().inflate(R.menu.workout_list_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                this.getSupportFragmentManager().popBackStackImmediate();
                this.removePreferences();
                break;
            case R.id.action_addWorkout:
                this.addWorkout();
                this.removePreferences();
                break;
            case R.id.action_editWorkout:
                this.editWorkout();
                this.removePreferences();
                break;
            case R.id.action_removeWorkout:
                this.removeWorkout();
                this.removePreferences();
                break;
            case R.id.action_manageExercises:
                this.manageExercises();
                this.removePreferences();
                break;
            case R.id.action_preferences:
                // confirm working
                if (currentFragment != null) {
                    getSupportFragmentManager().beginTransaction().remove(currentFragment).commit();
                }
                currentFragment = null;
                this.preferences();
                break;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem removeWorkout = menu.findItem(R.id.action_removeWorkout);
        if (removeWorkout != null) {
            removeWorkout.setVisible(isItemSelected);
        }
        MenuItem editWorkout = menu.findItem(R.id.action_editWorkout);
        if (editWorkout != null) {
            editWorkout.setVisible(isItemSelected);
        }
        return true;
    }

    public void addWorkout() {
        if (this.mTwoPane) {
            AddWorkoutFragment addWorkoutFragment = new AddWorkoutFragment();
            addWorkoutFragment.setCallback(this);
            currentFragment = addWorkoutFragment;
            this.getSupportFragmentManager().beginTransaction().replace(R.id.workout_detail_container, addWorkoutFragment).commit();
        } else {
            startActivity(new Intent(this, AddWorkoutActivity.class));
        }
        this.isPreferencesSelected = false;
    }

    private void editWorkout() {
        ((WorkoutDetailFragment)currentFragment).editWorkout();
    }

    public void removeWorkout() {
        WorkoutDetailFragment workoutDetailFragment;
        WorkoutListActivity.removeWorkout(getApplicationContext(), itemSelected);
        this.updateAdapter();
        this.currentFragment = (workoutDetailFragment = (WorkoutDetailFragment)(this.getSupportFragmentManager().findFragmentById(R.id.workout_detail_container)));
        this.getSupportFragmentManager().beginTransaction().remove(workoutDetailFragment).commit();
        this.isItemSelected = false;
        this.itemSelected = "";
        this.isPreferencesSelected = false;
        this.invalidateOptionsMenu();
    }

    public static void removeWorkout(Context context, String workoutID) {
        new DatabaseHelper(context).deleteWorkout(workoutID);
    }


    private void manageExercises() {
        if (this.mTwoPane) {
            ManageExercisesFragment manageExercisesFragment;
            this.currentFragment = (manageExercisesFragment = new ManageExercisesFragment());
            this.getSupportFragmentManager().beginTransaction().replace(R.id.workout_detail_container, manageExercisesFragment).commit();
        } else {
            startActivity(new Intent(this, ManageExercisesActivity.class));
        }
        this.isItemSelected = false;
        this.itemSelected = "";
        this.isPreferencesSelected = false;
        this.invalidateOptionsMenu();
    }


    private void preferences() {
        this.isItemSelected = false;
        this.itemSelected = "";
        this.isPreferencesSelected = true;
        this.invalidateOptionsMenu();
        if (this.mTwoPane) {
            workoutPreferencesFragment = new WorkoutPreferencesFragment();
            getFragmentManager().beginTransaction().replace(R.id.workout_detail_container, this.workoutPreferencesFragment).commit();
        } else {
            startActivity(new Intent(this, WorkoutPreferencesActivity.class));
        }
    }

    private void removePreferences() {
        if (isPreferencesSelected) {
            getFragmentManager().beginTransaction().remove(this.workoutPreferencesFragment).commit();
            isPreferencesSelected = false;
        }
    }


    public void updateAdapter() {
        WorkoutListFragment workoutListFragment = (WorkoutListFragment)(this.getSupportFragmentManager().findFragmentById(R.id.workout_list));
        if (workoutListFragment != null) {
            workoutListFragment.updateAdapter();
        }
    }
}

