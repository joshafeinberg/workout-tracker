package com.joshafeinberg.workouttracker;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;

public class WorkoutPreferencesActivity extends FragmentActivity {

    public WorkoutPreferencesActivity() {

    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_workout_detail);


        if (getActionBar() != null) {
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }

        if (savedInstanceState == null) {
            WorkoutPreferencesFragment workoutPreferencesFragment = new WorkoutPreferencesFragment();
            getFragmentManager().beginTransaction().replace(R.id.workout_detail_container, workoutPreferencesFragment).commit();
        }
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            NavUtils.navigateUpTo(this, new Intent(this, WorkoutListActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(menuItem);

    }
}

